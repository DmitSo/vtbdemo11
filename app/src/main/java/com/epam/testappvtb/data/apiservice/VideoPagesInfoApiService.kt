package com.epam.testappvtb.data.apiservice

import com.epam.testappvtb.data.dto.VideoPagesInfoResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming

interface VideoPagesInfoApiService {

    @GET("test/item")
    fun getVideoPagesInfoResult(): Single<Response<VideoPagesInfoResponse>>
}