package com.epam.testappvtb.data.repository

import com.epam.testappvtb.data.apiservice.VideoPageApiService
import javax.inject.Inject

class VideoPageRepository @Inject constructor(
    val videoPagesApiService: VideoPageApiService
) {

    fun loadVideoFromUrl(url: String) = videoPagesApiService.loadVideoFromUrl(url)
}