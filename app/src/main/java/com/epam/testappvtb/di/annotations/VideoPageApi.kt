package com.epam.testappvtb.di.annotations

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class VideoPageApi