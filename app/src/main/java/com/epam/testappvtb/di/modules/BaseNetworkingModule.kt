package com.epam.testappvtb.di.modules

import com.epam.testappvtb.BuildConfig
import com.epam.testappvtb.utils.ignoreSecuritySertificates
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.inject.Singleton
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

@Module
@InstallIn(SingletonComponent::class)
class BaseNetworkingModule {

    @Provides
    @Singleton
    fun provideBaseGson() = GsonBuilder()
        .create()

    @Provides
    @Singleton
    fun provideBaseOkHttpClientBuilder() = OkHttpClient.Builder()

    @Provides
    @Singleton
    fun provideBaseOkHttpClient(baseOkHttpClientBuilder: OkHttpClient.Builder) =
        baseOkHttpClientBuilder
            .apply {
                if (BuildConfig.DEBUG) {
                    addNetworkInterceptor(HttpLoggingInterceptor()
                        .apply { level = HttpLoggingInterceptor.Level.BODY })
                }
            }
            .build()

    @Provides
    @Singleton
    fun provideBaseRetrofitBuilder(baseOkHttpClient: OkHttpClient) =
        Retrofit.Builder()
            .client(baseOkHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
}