package com.epam.testappvtb.di.modules

import com.epam.testappvtb.data.apiservice.VideoPageApiService
import com.epam.testappvtb.di.annotations.VideoPageApi
import com.epam.testappvtb.utils.ignoreSecuritySertificates
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ViewModelComponent::class)
class VideoPageApiModule {

    @Provides
    @Singleton
    @VideoPageApi
    fun getVideoPageRetrofit(baseBuilder: Retrofit.Builder) = baseBuilder.build()

    @Provides
    @Singleton
    fun getVideoPageApi(@VideoPageApi retrofit: Retrofit) = retrofit.create(VideoPageApiService::class.java)
}