package com.epam.testappvtb.di.modules

import com.epam.testappvtb.data.apiservice.VideoPagesInfoApiService
import com.epam.testappvtb.di.annotations.VideoPagesInfoApi
import com.epam.testappvtb.utils.ignoreSecuritySertificates
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class VideoPagesInfoApiModule {

    @Provides
    @Singleton
    @VideoPagesInfoApi
    fun provideVideoPagesInfoBaseUrl() = "https://89.208.230.60/"

    @Provides
    @Singleton
    @VideoPagesInfoApi
    fun getVideoPageOkHttp(baseOkHttpClientBuilder: OkHttpClient.Builder) =
        baseOkHttpClientBuilder
            .ignoreSecuritySertificates()
            .build()

    @Provides
    @Singleton
    @VideoPagesInfoApi
    fun provideVideoPagesInfoRetrofit(
        baseRetrofitBuilder: Retrofit.Builder,
        @VideoPagesInfoApi videoPageOkHttpClient: OkHttpClient,
        @VideoPagesInfoApi baseUrl: String
    ) = baseRetrofitBuilder
        .client(videoPageOkHttpClient)
        .baseUrl(baseUrl)
        .build()

    @Provides
    @Singleton
    fun provideVideoPagesInfoApiService(
        @VideoPagesInfoApi retrofit: Retrofit
    ) = retrofit.create(VideoPagesInfoApiService::class.java)
}