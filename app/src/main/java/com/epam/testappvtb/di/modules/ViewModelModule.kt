package com.epam.testappvtb.di.modules

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.epam.testappvtb.di.ViewModelProviderFactory
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import androidx.lifecycle.ViewModel

import com.epam.testappvtb.di.annotations.ViewModelKey
import com.epam.testappvtb.presentation.feature.pagescontainer.VideoPagesContainerViewModel

import dagger.multibindings.IntoMap


@Module
@InstallIn(SingletonComponent::class)
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(VideoPagesContainerViewModel::class)
    abstract fun bindsAuthViewModel(viewModel: VideoPagesContainerViewModel?): ViewModel?

    @Binds
    abstract fun bindViewModelFactory(viewModelProviderFactory: ViewModelProviderFactory?): ViewModelProvider.Factory?

}