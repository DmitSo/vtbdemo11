package com.epam.testappvtb.domain.pojo

import com.google.gson.annotations.SerializedName

data class VideoPagesInfoModel(

    val firstVideoUrl: String,

    val secondVideoUrl: String,

    val thirdVideoUrl: String,

    val fourthVideoUrl: String
)