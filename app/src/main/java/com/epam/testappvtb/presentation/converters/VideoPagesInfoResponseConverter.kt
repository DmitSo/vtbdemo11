package com.epam.testappvtb.presentation.converters

import com.epam.testappvtb.data.dto.VideoPagesInfoResponse
import com.epam.testappvtb.domain.pojo.VideoPagesInfoModel

fun VideoPagesInfoResponse.toModel() = VideoPagesInfoModel(
    results.singleVideoUrl,
    results.splitVVideoUrl,
    results.splitHVideoUrl,
    results.srcVideoUrl
)
