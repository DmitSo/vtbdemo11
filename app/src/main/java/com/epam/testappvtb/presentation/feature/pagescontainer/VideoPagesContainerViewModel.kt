package com.epam.testappvtb.presentation.feature.pagescontainer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.epam.testappvtb.data.apiservice.VideoPagesInfoApiService
import com.epam.testappvtb.data.repository.VideoPageRepository
import com.epam.testappvtb.di.annotations.VideoPagesInfoApi
import com.epam.testappvtb.domain.pojo.VideoPagesInfoModel
import com.epam.testappvtb.presentation.base.BaseViewModel
import com.epam.testappvtb.presentation.converters.toModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class VideoPagesContainerViewModel @Inject constructor(
    val infoApiService: VideoPagesInfoApiService
): BaseViewModel() {

    private val _videoPagesInfo = MutableLiveData<VideoPagesInfoModel>()
    val videoPagesInfo: LiveData<VideoPagesInfoModel> = _videoPagesInfo

    fun requestVideoPagesInfo() {
        makeCall(infoApiService.getVideoPagesInfoResult(), {

            // TODO
            _videoPagesInfo.postValue(it.body()?.toModel())
        })
    }
}